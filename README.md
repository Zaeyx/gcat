*What is Gcat?*

Gcat is a simple and easy to use Proof Of Concept (POC) written in python 2.7; which provides a shell to a remote host.  Gcat uses gmail as the channel through which communication takes place between the implant and client.  That’s the big things that makes it special.

*Why on earth would you use Gmail as the communication pipe?*

Because organizations and governments will block anything and everything they can find that they deem malicious.  But who actually restricts access to productivity applications like gmail at a network level?  It certainly does happen in some incredibly restrictive environments.  But for the most part this communication channel should always be open.  Additionally, the traffic flowing from the implant to the command and control is going to look benign.  That is, even if the traffic was monitored at the network level; all the security team would see is someone checking their email.  It doesn’t beacon back to a domain that can be blacklisted as potentially malicious.  It doesn’t look like anything but a mail client accessing mail.google.com (a domain that nearly everyone trusts).  

*I’m sold, how does it work?*

It’s actually quite straight forward, and I’ll show example usage in just a minute.  But the 30,000ft view is this:

Register a throwaway gmail account
configure the leading variables in gcat.py
username (gmail)
password (gmail)
Select an ID for your campaign (just a randomly selected number)
Launch the command and control client
Place the implant
Every time the client is fed a command from stdin
The command is labeled with the campaign ID and passed to the gmail inbox
The implant regularly beacons to the inbox in search of new messages
If it finds a message with its campaign ID
It will execute the command
And pass back stdin and stderr as a tuple
The client regularly beacons to the inbox in search of new responses
If it finds a response from the implant
It will feed the tuple (stdin, stderr) the stdout of its terminal
Any shortcomings I should know about?

*Well, for starters, it is currently a proof of concept.  So, yeah.. it’s not a magical, beautiful, finished product yet.*

The biggest thing to watch out for, is that the implant will always execute the last command it sees in the inbox.  That is, if you have an implant with some ID X, and you’re sending it commands, but then it suddenly dies.  If you create a new implant with the same ID (X), when it runs it will execute whatever command you send the previous implant last (the top email with ID (X) in the subject).

This is because we have no real session mechanisms built in yet.  But it’s really no big deal.  This is just a warning that you are dealing with POC code so don’t expect a camaro quite yet!
